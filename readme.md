## Use
~~~~
<?php
include 'vendors/autoload.php';
//Url and Exceptions are available as helpers
//Move Url to utils?
~~~~

## Part of simple app

Should automatically be included if using composer to set up the project

Otherwise add a service line to config/app.php services array

```
'Smorken\Http\HttpService'
```