<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/25/14
 * Time: 7:47 AM
 */

namespace Smorken\Http;


use Smorken\Service\Service;

class UrlService  extends Service
{

    public function start()
    {
        $this->name = 'url';
    }

    public function load()
    {
        $this->app->instance($this->getName(), function($c) {
            return new Url($_SERVER);
        });
    }
}