<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/25/14
 * Time: 6:54 AM
 * Shamelessly borrowed from Laravel 4 and Symfony 2 Request
 */

namespace Smorken\Http;


class Url {

    protected $baseUrl;
    protected $requestUri;
    protected $pathInfo;
    protected $basePath;
    protected $server;

    public function __construct($server_vars)
    {
        $this->server = $server_vars;
    }

    /**
     * Generate a absolute URL to the given path.
     *
     * @param  string  $path
     * @param  mixed  $extra
     * @param  bool  $secure
     * @return string
     */
    public function to($path, $extra = array(), $secure = null)
    {
        // First we will check if the URL is already a valid URL. If it is we will not
        // try to generate a new one but will simply return the URL as is, which is
        // convenient since developers do not always have to check if it's valid.
        if ($this->isValidUrl($path)) return $path;

        $tail = implode('/', array_map(
                'rawurlencode', (array) $extra)
        );

        $path = $this->trimBase($path);

        // Once we have the scheme we will compile the "tail" by collapsing the values
        // into a single string delimited by slashes. This just makes it convenient
        // for passing the array of parameters to this URL as a list of segments.
        $root = $this->getSchemeAndHttpHost($secure) . $this->removeIndex($this->getRootUrl());

        return ltrim($this->trimUrl($root, $path, $tail), '/');
    }

    /**
     * Generates the relative URL to the given path.
     *
     * @param $path
     * @param array $extra
     * @return string
     */
    public function relative($path, $extra = array())
    {
        $tail = implode('/', array_map(
                'rawurlencode', (array) $extra)
        );
        $path = $this->trimBase($this->removeIndex($path));
        $root = $this->removeIndex($this->getRootUrl());
        $r = stripos($path, $root) !== false ? str_replace($root, '', $path) : $path;
        return ltrim($this->trimUrl($r, $tail), '/');
    }

    public function asset($path)
    {
        return $this->to($path);
    }

    /**
     * Remove the index.php file from a path.
     *
     * @param  string  $root
     * @return string
     */
    protected function removeIndex($root)
    {
        $i = 'index.php';

        $r = stripos($root, $i) !== false ? str_replace($i, '', $root) : $root;
        return $r;
    }

    public function getMethod()
    {
        return strtoupper(array_get($this->server, 'REQUEST_METHOD', 'GET'));
    }

    /**
     * Gets the scheme and HTTP host.
     *
     * If the URL was called with basic authentication, the user
     * and the password are not added to the generated string.
     *
     * @param null $secure
     * @return string The scheme and HTTP host
     */
    public function getSchemeAndHttpHost($secure = null)
    {
        $scheme = $this->scheme($secure);
        return $scheme . $this->getHttpHost();
    }

    /**
     * Returns the HTTP host being requested.
     *
     * The port name will be appended to the host if it's non-standard.
     *
     * @return string
     *
     * @api
     */
    public function getHttpHost()
    {
        $scheme = $this->getScheme();
        $port   = $this->getPort();

        if (('http' === $scheme && $port == 80) || ('https' === $scheme && $port == 443)) {
            return $this->getHost();
        }

        return $this->getHost().':'.$port;
    }

    /**
     * Returns the port on which the request is made.
     * @return string
     */
    public function getPort()
    {
        return array_get($this->server, 'SERVER_PORT', 80);
    }

    /**
     * Returns the host name.
     *
     * @return string
     *
     * @throws \UnexpectedValueException when the host name is invalid
     */
    public function getHost()
    {
        if (!$host = array_get($this->server, 'SERVER_NAME')) {
            $host = array_get($this->server, 'SERVER_ADDR', '');
        }

        // trim and remove port number from host
        // host is lowercase as per RFC 952/2181
        $host = strtolower(preg_replace('/:\d+$/', '', trim($host)));

        // as the host can come from the user (HTTP_HOST and depending on the configuration, SERVER_NAME too can come from the user)
        // check that it does not contain forbidden characters (see RFC 952 and RFC 2181)
        if ($host && !preg_match('/^\[?(?:[a-zA-Z0-9-:\]_]+\.?)+$/', $host)) {
            throw new \UnexpectedValueException(sprintf('Invalid Host "%s"', $host));
        }

        return $host;
    }

    /**
     * Returns the path being requested relative to the executed script.
     *
     * The path info always starts with a /.
     *
     * Suppose this request is instantiated from /mysite on localhost:
     *
     *  * http://localhost/mysite              returns an empty string
     *  * http://localhost/mysite/about        returns '/about'
     *  * http://localhost/mysite/enco%20ded   returns '/enco%20ded'
     *  * http://localhost/mysite/about?var=1  returns '/about'
     *
     * @return string The raw path (i.e. not urldecoded)
     *
     * @api
     */
    public function getPathInfo()
    {
        if (null === $this->pathInfo) {
            $this->pathInfo = $this->preparePathInfo();
        }

        return $this->pathInfo;
    }

    /**
     * Returns the root path from which this request is executed.
     *
     * Suppose that an index.php file instantiates this request object:
     *
     *  * http://localhost/index.php         returns an empty string
     *  * http://localhost/index.php/page    returns an empty string
     *  * http://localhost/web/index.php     returns '/web'
     *  * http://localhost/we%20b/index.php  returns '/we%20b'
     *
     * @return string The raw path (i.e. not urldecoded)
     *
     * @api
     */
    public function getBasePath()
    {
        if (null === $this->basePath) {
            $this->basePath = $this->prepareBasePath();
        }

        return $this->basePath;
    }

    /**
     * Generates a normalized URI for the Request.
     *
     * @return string A normalized URI for the Request
     *
     * @see getQueryString()
     *
     * @api
     */
    public function getUri()
    {
        if (null !== $qs = $this->getQueryString()) {
            $qs = '?'.$qs;
        }

        return $this->getSchemeAndHttpHost().$this->removeIndex($this->getBaseUrl()).$this->getPathInfo().$qs;
    }

    /**
     * Generates a normalized URI for the given path.
     *
     * @param string $path A path to use instead of the current one
     *
     * @return string The normalized URI for the path
     *
     * @api
     */
    public function getUriForPath($path)
    {
        return $this->getSchemeAndHttpHost().$this->removeIndex($this->getBaseUrl()).$path;
    }

    /**
     * Generates the normalized query string for the Request.
     *
     * It builds a normalized query string, where keys/value pairs are alphabetized
     * and have consistent escaping.
     *
     * @return string|null A normalized query string for the Request
     *
     * @api
     */
    public function getQueryString()
    {
        $qs = static::normalizeQueryString(array_get($this->server, 'QUERY_STRING'));

        return '' === $qs ? null : $qs;
    }

    /**
     * Normalizes a query string.
     *
     * It builds a normalized query string, where keys/value pairs are alphabetized,
     * have consistent escaping and unneeded delimiters are removed.
     *
     * @param string $qs Query string
     *
     * @return string A normalized query string for the Request
     */
    public static function normalizeQueryString($qs)
    {
        if ('' == $qs) {
            return '';
        }

        $parts = array();
        $order = array();

        foreach (explode('&', $qs) as $param) {
            if ('' === $param || '=' === $param[0]) {
                // Ignore useless delimiters, e.g. "x=y&".
                // Also ignore pairs with empty key, even if there was a value, e.g. "=value", as such nameless values cannot be retrieved anyway.
                // PHP also does not include them when building _GET.
                continue;
            }

            $keyValuePair = explode('=', $param, 2);
            $p = get_purifier();
            // GET parameters, that are submitted from a HTML form, encode spaces as "+" by default (as defined in enctype application/x-www-form-urlencoded).
            // PHP also converts "+" to spaces when filling the global _GET or when using the function parse_str. This is why we use urldecode and then normalize to
            // RFC 3986 with rawurlencode.
            $parts[] = isset($keyValuePair[1]) ?
                rawurlencode(urldecode($keyValuePair[0])).'=' . $p->purify(rawurlencode(urldecode($keyValuePair[1]))) :
                rawurlencode(urldecode($keyValuePair[0]));
            $order[] = urldecode($keyValuePair[0]);
        }

        array_multisort($order, SORT_ASC, $parts);

        return implode('&', $parts);
    }

    /**
     * Get the base URL for the request.
     *
     * @param  string $root
     * @internal param string $scheme
     * @return string
     */
    protected function getRootUrl($root = null)
    {
        if (is_null($root))
        {
            $root = $this->getBaseUrl();
        }

        return $root;
    }

    /**
     * Determine if the given path is a valid URL.
     *
     * @param  string  $path
     * @return bool
     */
    public function isValidUrl($path)
    {
        if (starts_with($path, array('#', '//', 'mailto:', 'tel:'))) return true;

        return filter_var($path, FILTER_VALIDATE_URL) !== false;
    }

    /**
     * Format the given URL segments into a single URL.
     *
     * @param  string  $root
     * @param  string  $path
     * @param  string  $tail
     * @return string
     */
    protected function trimUrl($root, $path, $tail = '')
    {
        $trim = function($v) { return ($v ? trim($v, '/') : false); };
        $parts = array_filter(array($root, $path, $tail), 'strlen');
        $trimmed = array_map($trim, $parts);
        return '/' . implode('/', $trimmed);
        //return '/' . trim($root.'/'.trim($path.'/'.$tail, '/'), '/');
    }

    /**
     * Strip the base url from the path
     *
     * @param $path
     * @return string
     */
    public function trimBase($path)
    {
        $base = trim($this->getBaseUrl(), '/');
        $path = trim($path, '/');
        if (substr($path, 0, strlen($base)) === $base) {
            $path = substr($path, strlen($base));
        }
        return '/' . $path;
    }

    /**
     * Get the scheme for a raw URL.
     *
     * @param  bool    $secure
     * @return string
     */
    protected function scheme($secure)
    {
        if (is_null($secure))
        {
            return $this->getScheme() . '://';
        }
        else
        {
            return $secure ? 'https://' : 'http://';
        }
    }

    /**
     * Returns the requested URI.
     *
     * @return string The raw URI (i.e. not urldecoded)
     *
     * @api
     */
    public function getRequestUri()
    {
        if (null === $this->requestUri) {
            $this->requestUri = $this->prepareRequestUri();
        }

        return $this->requestUri;
    }


    /**
     * Returns the root URL from which this request is executed.
     *
     * The base URL never ends with a /.
     *
     * This is similar to getBasePath(), except that it also includes the
     * script filename (e.g. index.php) if one exists.
     *
     * @return string The raw URL (i.e. not urldecoded)
     *
     * @api
     */
    public function getBaseUrl()
    {
        if (null === $this->baseUrl) {
            $this->baseUrl = $this->prepareBaseUrl();
        }

        return $this->baseUrl;
    }

    public function getRealFilename()
    {
        $filename = array_get($this->server, 'SCRIPT_FILENAME');
        $added = array_get($this->server, 'DOCUMENT_ROOT') . array_get($this->server, 'PHP_SELF');
        if ($filename === $added) {
            return $filename;
        }
        return $added;
    }

    /*
     * The following methods are derived from code of the Zend Framework (1.10dev - 2010-01-24)
     *
     * Code subject to the new BSD license (http://framework.zend.com/license/new-bsd).
     *
     * Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
     */

    protected function prepareRequestUri()
    {
        $requestUri = '';
        if (array_get($this->server, 'IIS_WasUrlRewritten') == '1' && array_get($this->server, 'UNENCODED_URL') != '') {
            // IIS7 with URL Rewrite: make sure we get the unencoded URL (double slash problem)
            $requestUri = array_get($this->server, 'UNENCODED_URL');
        } elseif (array_get($this->server, 'REQUEST_URI')) {
            $requestUri = array_get($this->server, 'REQUEST_URI');
            // HTTP proxy reqs setup request URI with scheme and host [and port] + the URL path, only use URL path
            $schemeAndHttpHost = $this->getSchemeAndHttpHost();
            if (strpos($requestUri, $schemeAndHttpHost) === 0) {
                $requestUri = substr($requestUri, strlen($schemeAndHttpHost));
            }
        } elseif (array_get($this->server, 'ORIG_PATH_INFO')) {
            // IIS 5.0, PHP as CGI
            $requestUri = array_get($this->server, 'ORIG_PATH_INFO');
            if ('' != array_get($this->server, 'QUERY_STRING')) {
                $requestUri .= '?'.array_get($this->server, 'QUERY_STRING');
            }
        }
        $requestUri = get_purifier()->purify($requestUri);
        $this->server['REQUEST_URI'] = $requestUri;

        return $requestUri;
    }

    /**
     * Prepares the base URL.
     * borrowed from Symfony Request
     * @return string
     */
    protected function prepareBaseUrl()
    {
        $filename = basename(array_get($this->server, 'SCRIPT_FILENAME'));

        if (basename(array_get($this->server, 'SCRIPT_NAME')) === $filename) {
            $baseUrl = array_get($this->server, 'SCRIPT_NAME');
        } elseif (basename(array_get($this->server, 'PHP_SELF')) === $filename) {
            $baseUrl = array_get($this->server, 'PHP_SELF');
        } elseif (basename(array_get($this->server, 'ORIG_SCRIPT_NAME')) === $filename) {
            $baseUrl = array_get($this->server, 'ORIG_SCRIPT_NAME'); // 1and1 shared hosting compatibility
        } else {
            // Backtrack up the script_filename to find the portion matching
            // php_self
            $path    = array_get($this->server, 'PHP_SELF', '');
            $file    = $this->trimUrl(array_get($this->server, 'DOCUMENT_ROOT', '/'), array_get($this->server, 'SCRIPT_NAME', ''));
            //$file    = str_replace('//', '/', array_get($this->server, 'SCRIPT_FILENAME', ''));
            $segs    = explode('/', trim($file, '/'));
            $segs    = array_reverse($segs);
            $index   = 0;
            $last    = count($segs);
            $baseUrl = '';
            do {
                $seg     = $segs[$index];
                $ext = pathinfo($seg, PATHINFO_EXTENSION);
                if ($ext && $index < $last) {
                    $index++;
                    $seg = $segs[$index];
                }
                $baseUrl = '/'.$seg.$baseUrl;
                ++$index;
            } while ($last > $index && (false !== $pos = strpos($path, $baseUrl)) && 0 != $pos);
        }

        // Does the baseUrl have anything in common with the request_uri?
        $requestUri = $this->getRequestUri();
        $baseUrl = get_purifier()->purify($baseUrl);
        if ($baseUrl && false !== $prefix = $this->getUrlencodedPrefix($requestUri, $baseUrl)) {
            // full $baseUrl matches
            return $prefix;
        }
        if ($baseUrl && false !== $prefix = $this->getUrlencodedPrefix(rtrim($requestUri, '/' . DIRECTORY_SEPARATOR), rtrim(dirname($baseUrl), '/'.DIRECTORY_SEPARATOR))) {
            // directory portion of $baseUrl matches
            return rtrim($prefix, '/'.DIRECTORY_SEPARATOR);
        }
        $truncatedRequestUri = $requestUri;
        if (false !== $pos = strpos($requestUri, '?')) {
            $truncatedRequestUri = substr($requestUri, 0, $pos);
        }
        $basename = basename($baseUrl);
        if (empty($basename) || !strpos(rawurldecode($truncatedRequestUri), $basename)) {
            // no match whatsoever; set it blank
            return '';
        }
        // If using mod_rewrite or ISAPI_Rewrite strip the script filename
        // out of baseUrl. $pos !== 0 makes sure it is not matching a value
        // from PATH_INFO or QUERY_STRING
        if (strlen($requestUri) >= strlen($baseUrl) && (false !== $pos = strpos($requestUri, $baseUrl)) && $pos !== 0) {
            $baseUrl = substr($requestUri, 0, $pos + strlen($baseUrl));
        }
        return rtrim($baseUrl, '/'.DIRECTORY_SEPARATOR);
    }

    /*
     * Returns the prefix as encoded in the string when the string starts with
     * the given prefix, false otherwise.
     *
     * @param string $string The urlencoded string
     * @param string $prefix The prefix not encoded
     *
     * @return string|false The prefix as it is encoded in $string, or false
     */
    protected function getUrlencodedPrefix($string, $prefix)
    {
        if ($prefix && $string !== $prefix) {
            $pos = strpos(rawurldecode($string), $prefix);
            if (0 !== $pos) {
                return false;
            }
        }

        $len = strlen($prefix);

        if (preg_match(sprintf('#^(%%[[:xdigit:]]{2}|.){%d}#', $len), $string, $match)) {
            return $match[0];
        }

        return false;
    }

    /**
     * Prepares the base path.
     *
     * @return string base path
     */
    protected function prepareBasePath()
    {
        $filename = basename(array_get($this->server, 'SCRIPT_FILENAME'));
        $baseUrl = $this->getBaseUrl();
        if (empty($baseUrl)) {
            return '';
        }

        if (basename($baseUrl) === $filename) {
            $basePath = dirname($baseUrl);
        } else {
            $basePath = $baseUrl;
        }

        if ('\\' === DIRECTORY_SEPARATOR) {
            $basePath = str_replace('\\', '/', $basePath);
        }

        return rtrim($basePath, '/');
    }

    /**
     * Prepares the path info.
     *
     * @return string path info
     */
    protected function preparePathInfo()
    {
        $baseUrl = $this->getBaseUrl();

        if (null === ($requestUri = $this->getRequestUri())) {
            return '/';
        }

        $pathInfo = '/';

        // Remove the query string from REQUEST_URI
        if ($pos = strpos($requestUri, '?')) {
            $requestUri = substr($requestUri, 0, $pos);
        }

        $pathInfo = substr($requestUri, strlen($baseUrl));
        if (null !== $baseUrl && (false === $pathInfo || '' === $pathInfo)) {
            // If substr() returns false then PATH_INFO is set to an empty string
            return '/';
        } elseif (null === $baseUrl) {
            return $requestUri;
        }
        return (string) $pathInfo;
    }

    public function isSecure()
    {
        $https = array_get($this->server, 'HTTPS');
        $https_port = array_get($this->server, 'SERVER_PORT');
        return (!empty($https) && 'off' !== strtolower($https)) || $https_port == 443;
    }

    public function getScheme()
    {
        return $this->isSecure() ? 'https': 'http';
    }
}
