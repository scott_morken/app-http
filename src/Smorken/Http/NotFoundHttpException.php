<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/25/14
 * Time: 8:24 AM
 */

namespace Smorken\Http;


class NotFoundHttpException extends HttpException {

    public function __construct($message, \Exception $previous = null, $code = 0)
    {
        parent::__construct(404, $message, $previous, $code);
    }
} 