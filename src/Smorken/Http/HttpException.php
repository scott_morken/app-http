<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/25/14
 * Time: 8:18 AM
 */

namespace Smorken\Http;


class HttpException extends \RuntimeException {

    protected $status;

    public function __construct($status, $message = null, \Exception $previous = null, $code = 0)
    {
        $this->status = $status;
        http_response_code($status);
        parent::__construct($message, $code, $previous);
    }

    public function getStatus()
    {
        return $this->status;
    }
} 