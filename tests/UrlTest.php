<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/5/15
 * Time: 5:57 PM
 */
class UrlTest extends PHPUnit_Framework_TestCase {

    public function testRelativeBase()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $relative = $sut->relative('/');
        $this->assertEquals('', $relative);
    }

    public function testTrimRootBase()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $trimmed = $sut->trimBase('/system-status');
        $this->assertEquals('/', $trimmed);
    }

    public function testAssetBase()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $asset = $sut->asset('assets/js/foo.js');
        $this->assertEquals('http://staging.pc.maricopa.edu/system-status/assets/js/foo.js', $asset);
    }

    public function testAssetLeadingSlashBase()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $asset = $sut->asset('/assets/js/foo.js');
        $this->assertEquals('http://staging.pc.maricopa.edu/system-status/assets/js/foo.js', $asset);
    }

    public function testToBase()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $to = $sut->to('foo');
        $this->assertEquals('http://staging.pc.maricopa.edu/system-status/foo', $to);
    }

    public function testToFullBase()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $to = $sut->to('http://staging.pc.maricopa.edu/system-status/foo');
        $this->assertEquals('http://staging.pc.maricopa.edu/system-status/foo', $to);
    }

    public function testMatcherBaseInName()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBase());
        $request = '/system-status/admin/groups';
        $comp = $sut->relative($sut->trimBase($request));
        $this->assertEquals('admin/groups', $comp);
    }

    public function testMatcherLogio()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithBaseLocalhost());
        $request = '/public/logio.php';
        $comp = $sut->relative($sut->trimBase($request));
        $this->assertEquals('logio.php', $comp);
    }

    public function testMatcherBase()
    {
        $s = $this->getServerArrayWithBaseLocalhost();
        $s['REQUEST_URI'] = '/public';
        $s['SCRIPT_NAME'] = '/public/index.php';
        $s['SCRIPT_FILENAME'] = '/data/sas/public/index.php';
        $s['PHP_SELF'] = '/public/index.php';
        $sut = new \Smorken\Http\Url($s);
        $request = '/public';
        $comp = $sut->relative($sut->trimBase($request));
        $this->assertEquals('', $comp);
    }

    public function testMatcherSimilarName()
    {
        $s = $this->getServerArrayWithBaseLocalhost();
        $s['REQUEST_URI'] = '/public/prefixes.ajax?term_id=4156';
        $s['SCRIPT_NAME'] = '/public/prefixes.ajax';
        $s['SCRIPT_FILENAME'] = '/data/sas/public/something/something/something.php';
        $s['PHP_SELF'] = '/public/prefixes.ajax';
        $sut = new \Smorken\Http\Url($s);
        $request = '/public/whitelist.ajax?term_id=4156';
        $comp = $sut->relative($sut->trimBase($request));
        $this->assertEquals('whitelist.ajax?term_id=4156', $comp);
    }

    public function testBasePathInTo()
    {
        $s = $this->getServerArrayWithBaseLocalhost();
        $s['REQUEST_URI'] = '/public/';
        $s['SCRIPT_NAME'] = '/public/index.php';
        $s['SCRIPT_FILENAME'] = '/data/sas/public/index.php';
        $s['PHP_SELF'] = '/public/index.php';
        $sut = new \Smorken\Http\Url($s);
        $request = '/public/';
        $comp = $sut->to($request);
        $this->assertEquals('http://127.0.0.1:8080/public/', $comp);
    }

    public function testRelativeDocRoot()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithDocRoot());
        $relative = $sut->relative('/foo/bar');
        $this->assertEquals('foo/bar', $relative);
    }

    public function testTrimRootDocRoot()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithDocRoot());
        $trimmed = $sut->trimBase('/');
        $this->assertEquals('/', $trimmed);
    }

    public function testAssetDocRoot()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithDocRoot());
        $asset = $sut->asset('assets/js/foo.js');
        $this->assertEquals('http://127.0.0.1:8080/assets/js/foo.js', $asset);
    }

    public function testAssetLeadingSlashDocRoot()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithDocRoot());
        $asset = $sut->asset('/assets/js/foo.js');
        $this->assertEquals('http://127.0.0.1:8080/assets/js/foo.js', $asset);
    }

    public function testToDocRoot()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithDocRoot());
        $to = $sut->to('foo');
        $this->assertEquals('http://127.0.0.1:8080/foo', $to);
    }

    public function testToFullDocRoot()
    {
        $sut = new \Smorken\Http\Url($this->getServerArrayWithDocRoot());
        $to = $sut->to('http://127.0.0.1:8080/foo');
        $this->assertEquals('http://127.0.0.1:8080/foo', $to);
    }

    public function testGetRequestUriStipsTags()
    {
        $s = $this->getServerArrayWithDocRoot();
        $s['REQUEST_URI'] = '/foo/<script>alert(1);</script>';
        $sut = new \Smorken\Http\Url($s);
        $r = $sut->getRequestUri();
        $this->assertEquals('/foo/', $r);
    }

    protected function getServerArrayWithBase()
    {
        return [
            'HTTP_HOST' => 'staging.pc.maricopa.edu<script>alert(1);</script>',
            'HTTP_CONNECTION' => 'keep-alive',
            'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'HTTP_UPGRADE_INSECURE_REQUESTS' => '1',
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
            'HTTP_DNT' => '1',
            'HTTP_ACCEPT_ENCODING' => 'gzip, deflate, sdch',
            'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8',
            'HTTP_COOKIE' => '_ga=GA1.2.1426981424.1435615129',
            'PATH' => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
            'SERVER_SIGNATURE' => '',
            'SERVER_SOFTWARE' => 'Apache/2.4.7 (Ubuntu)',
            'SERVER_NAME' => 'staging.pc.maricopa.edu',
            'SERVER_ADDR' => '140.198.16.151',
            'SERVER_PORT' => '80',
            'REMOTE_ADDR' => '140.198.19.157',
            'DOCUMENT_ROOT' => '/www/pc',
            'REQUEST_SCHEME' => 'http',
            'CONTEXT_PREFIX' => '/system-status',
            'CONTEXT_DOCUMENT_ROOT' => '/www/ext/system-status/public',
            'SERVER_ADMIN' => 'scott.morken@phoenixcollege.edu',
            'SCRIPT_FILENAME' => '/www/ext/system-status/public/index.php',
            'REMOTE_PORT' => '42294',
            'GATEWAY_INTERFACE' => 'CGI/1.1',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'REQUEST_METHOD' => 'GET',
            'QUERY_STRING' => '',
            'REQUEST_URI' => '/system-status/',
            'SCRIPT_NAME' => '/system-status/index.php',
            'PHP_SELF' => '/system-status/index.php',
            'REQUEST_TIME_FLOAT' => '1438822411.746',
            'REQUEST_TIME' => '1438822411',
        ];
    }

    protected function getServerArrayWithDocRoot()
    {
        return [
            'DOCUMENT_ROOT' => '/data/project/someproject/public',
            'REMOTE_ADDR' => '127.0.0.1',
            'REMOTE_PORT' => '34150',
            'SERVER_SOFTWARE' => 'PHP 5.6.13 Development Server',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'SERVER_NAME' => '127.0.0.1',
            'SERVER_PORT' => '8080',
            'REQUEST_URI' => '/',
            'REQUEST_METHOD' => 'GET',
            'SCRIPT_NAME' => '/index.php',
            'SCRIPT_FILENAME' => '/data/project/someproject/public/index.php',
            'PHP_SELF' => '/index.php',
            'HTTP_HOST' => '127.0.0.1:8080<script>alert(1);</script>',
            'HTTP_CONNECTION' => 'keep-alive',
            'HTTP_CACHE_CONTROL' => 'max-age=0',
            'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'HTTP_UPGRADE_INSECURE_REQUESTS' => '1',
            'HTTP_USER_AGENT' => 'Mozilla/5.0',
            'HTTP_DNT' => '1',
            'HTTP_ACCEPT_ENCODING' => 'gzip, deflate, sdch',
            'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8',
            'HTTP_COOKIE' => 'XDEBUG_SESSION=PHPSTORM; PHPSESSID=44p13p257o25rpbp4137ssope0',
            'REQUEST_TIME_FLOAT' => 1444398666.0193,
            'REQUEST_TIME' => 1444398666,
        ];
    }

    protected function getServerArrayWithBaseLocalhost()
    {
        return array
        (
            'DOCUMENT_ROOT' => '/data/sas',
            'REMOTE_ADDR' => '127.0.0.1',
            'REMOTE_PORT' => '35537',
            'SERVER_SOFTWARE' => 'PHP 5.6.13 Development Server',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'SERVER_NAME' => '127.0.0.1',
            'SERVER_PORT' => '8080',
            'REQUEST_URI' => '/public/logio.php',
            'REQUEST_METHOD' => 'GET',
            'SCRIPT_NAME' => '/public/logio.php',
            'SCRIPT_FILENAME' => '/data/sas//data/projects/php/app-module/sas/router.php',
            'PHP_SELF' => '/public/logio.php',
            'HTTP_HOST' => '127.0.0.1:8080',
            'HTTP_CONNECTION' => 'keep-alive',
            'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'HTTP_UPGRADE_INSECURE_REQUESTS' => '1',
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36',
            'HTTP_DNT' => '1',
            'HTTP_REFERER' => 'http://127.0.0.1:8080/public',
            'HTTP_ACCEPT_ENCODING' => 'gzip, deflate, sdch',
            'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8',
            'HTTP_COOKIE' => 'XDEBUG_SESSION=PHPSTORM; PHPSESSID=5vapdbf2b2h8pvhk24l52svfo7',
            'REQUEST_TIME_FLOAT' => '1444405953.71',
            'REQUEST_TIME' => '1444405953',
        );
    }
}
